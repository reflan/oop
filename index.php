<?php
require_once("animal.php");
require_once("Frog.php");
require_once("Ape.php");

$sheep = new Animal("shaun");
$frog = new Frog("buduk");
$ape = new Ape("Kera Sakti");

echo "Name : ".$sheep->name."<br>"; // "shaun"
echo "Legs : ".$sheep->legs."<br>"; // 4
echo "Cold Blooded : ".$sheep->cold_blooded."<br><br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

echo "Name : ".$frog->name."<br>"; 
echo "Legs : ".$frog->legs."<br>"; 
echo "Cold Blooded : ".$frog->cold_blooded."<br>"; 
echo "Jump : ".$frog->jump()."<br><br>";

echo "Name : ".$ape->name."<br>"; 
echo "Legs : ".$ape->legs."<br>"; 
echo "Cold Blooded : ".$ape->cold_blooded."<br>"; 
echo "Yell : ".$ape->yell()."<br><br>"; 

?>